# Render pretty JSON in Terminal

- Add to ~/.bash_profile
```
alias pretty-json="python -mjson.tool"
```