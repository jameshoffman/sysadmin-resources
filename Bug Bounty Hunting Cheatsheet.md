# Identify targets

Find IP Space

* http://bgp.he.net
* ARIN
* RIPE
* http://reverse.report
* shodan.io

* DomLink: find other domains registered by the same organization

* *social*: find acquisitions
    * crunchbase: crunchbase.com/organization/tesla-motors/acquisitions
    * Burp "linked discovery": link spidering -> TBHM v3 @ 15min.

Finding sub-domains

* ~~Recon-ng~~ (wrapper for enumall.sh) sources are kinda deprecated now
* ~~sublist3r~~ was good, but now Amass + Subfinder took over

* massdns: very fast, more false positive
* gobuster: fast, manageable output

* use ALL.TXT: subdomains dictionary

# Map target

Port scans to find exposed services (need to dig ip addresses for these tools to work)

* nmap -sS -A -N -p- --script=http-title domain.com
* massscan

Visually identify websites

* EyeWitness

Identify the platform

* retire.js
* Builtwith browser plugin
* vulners.com
* wappalyzer

# Content discovery

For CMSs

* WPScan
* CMSmap

Services credentials bruteforcing

* brutespray

Directory brute-forcing, find unlisted files

* gobuster
* RobotsDisallowed
* DirBuster, SmartBurpBuster
* content_discovery_all.txt

*do not brute-force only top level*, explore subfolders -> less traveled roads

* tomnomnom/waybacksurls : when getting a 401/403 verify if file content is actually available in archive.org's cache

Javascript Spidering

* ZAP Ajax spider

* LinkFinder
* JSParser less good than link finder

Parameters bruteforcing

* parameth

# Find exploits and tactical fuzzing

XSS polyglot payload to evade escape rules

* Rsnake XSS Cheat sheet
* XSS Research
* Mathias Karlsson
* HackVault

Look for input vectors

* third party integrations(eg: facebook login, site displays facebook name, set your name to alert(1), then bingo!)
* File uploads, file names
* Any user provided data
* fake url parameters might be parsed, &dummy=dumb'+alert(1)+'
* login and forgot password forms

Blind XSS

* XSSHunter
* bXSS
* ezXSS

SWF files exploits

* Flashbang tool

IDOR
    
* look for ids, hashes, email, etc. 

Server side template injection

* TPLMap

SSRF

* Bypass ip filters by using alternate IP encodings
* dotless, octal, hex, etc.

* SSRF Bible

* cloud_metadata.txt

CSRF

* Burpy
* Look for critical functions like File upload, user profile edits, sensitive requests

CMDi
    
* Commix
* Backslash Powered Scanner burp plugin

SQLi 

* polyglot payloads: SLEEP(1) /*' or SLEEP(1) or '" or SLEEP(1) or " */
* SQLMap, TampScripts
* PentestMonkey's cheatsheets

File uploads

* Liffy
* File in the hole! talk

Logic flaws

* Substitutions (eg: in ecommerce cart, substitute cheaper article hash)
* Steps manipulations
* Parameters manipulation, negative values
* Application DOS, do not flood the server but check how it responds to different data sets

# Misconfigurations

SubDomain Takeover

* CNAME lapsed

* EdOverflow/can-i-take-over-xyz -> nice doc

* autoSubTakeover
* HostileSubBruteforcer
* tko-subs

AWS buckets

* S3Scanner
* sancastle
* web hacking 101 video 

Git

* gitrobber
* truffleHog

Bypass WAF, Akamai, Cloudflare

* Look for the orign or the dev to call directly the source

* dev.domain.com
* stage.domain.com
* ww1.domain.com, ww2, etc.

Privileges

* Autorize, on restricted pages/functions of a site
* Insecure direct object references: increment, decrement, substitute IDs
* HTTPS, ForceSSL script

# Mobile

Android

* Extract APKs: https://ibotpeaches.github.io/Apktool/
* Extract Java code from APKs: https://github.com/skylot/jadx
* Proxy: https://mitmproxy.org/
* Reverse apps: https://frida.re/
* Also explore ADB and logcat from Android SDK: 
    * https://developer.android.com/studio/command-line/adb
    * https://developer.android.com/studio/command-line/logcat

Ref: https://www.hackerone.com/blog/androidhackingmonth-intro-to-android-hacking


# Other ressources

Existing vulnerabilities from public disclosure databases

* exploit-db.com
* xssed.com
* punkspider
* xss.cx
* xssposed.org

*Issues might already be known, try to exploit it further or on other domains you discovered*

Tools

* [Seclists](https://github.com/danielmiessler/SecLists)
* [The Bug Hunter Methodology](https://github.com/jhaddix/tbhm)
* <http://bugbountyforum.com>
* xmind to organize bounty hunting informations  