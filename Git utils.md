# Handling global Git config

## Identity

git config --global user.name "John Doe"

git config --global user.email johndoe@example.com

## Global gitignore

git config --global core.excludesfile ~/.gitignore_global

vim ~/.gitignore_global

```
.DS_Store
.DS_Store?
._*
.Spotlight-V100
.Trashes
ehthumbs.db
Thumbs.db
```

## View config

git config --list

# Get status of all the git repositories in a folder

- Add to ~/.bash_profile

```
alias git-statuses="find . -maxdepth 1 -mindepth 1 -type d -exec sh -c '(echo {} && cd {} && git status -s && echo)' \;"
```

# Make a git-aware Terminal
Display git status and current branch in Terminal

## Download completion files
* curl -o ~/.git-prompt.sh https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
* curl -o ~/.git-completion.bash https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash


## Edit ~/.bash_profile

```
#!/bin/bash

source ~/.git-completion.bash
source ~/.git-prompt.sh

BLACK="\[\033[0;30m\]"
MAGENTA="\[\033[0;35m\]"
YELLOW="\[\033[0;33m\]"
BLUE="\[\033[34m\]"
GRAY="\[\033[1;90m\]"
CYAN="\[\033[0;36m\]"
GREEN="\[\033[0;32m\]"
GIT_PS1_SHOWDIRTYSTATE=true
export LS_OPTIONS='--color=auto'
export CLICOLOR='Yes'
export LSCOLORS=gxfxbEaEBxxEhEhBaDaCaD

export PS1=$BLACK"\u@\h"'$(
    if [[ $(__git_ps1) =~ \*\)$ ]]
    # a file has been modified but not added
    then echo "'$YELLOW'"$(__git_ps1 " (%s)")
    elif [[ $(__git_ps1) =~ \+\)$ ]]
    # a file has been added, but not commited
    then echo "'$MAGENTA'"$(__git_ps1 " (%s)")
    # the state is clean, changes are commited
    else echo "'$CYAN'"$(__git_ps1 " (%s)")
    fi)'$GRAY" \w"$BLACK": "
```  
    
## References
* https://gist.github.com/trey/2722934
* http://neverstopbuilding.com/gitpro
* http://stackoverflow.com/questions/31269059/set-git-bash-profile-ps1-to-color-branch-name-when-commits-have-not-been-pushed